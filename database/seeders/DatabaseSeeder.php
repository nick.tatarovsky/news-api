<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Post;
use App\Models\PostTranslation;
use App\Models\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            LanguageSeeder::class,
        ]);

        $posts = Post::factory(20)->create();

        $tags_ids = Tag::factory(100)->create()->pluck('id')->toArray();

        $posts->each(function ($post) use ($tags_ids) {
            PostTranslation::factory()->create(['post_id' => $post->id, 'language_id' => 1]);
            PostTranslation::factory()->create(['post_id' => $post->id, 'language_id' => 2]);
            PostTranslation::factory()->create(['post_id' => $post->id, 'language_id' => 3]);

            $post->tags()->attach(Arr::random($tags_ids, 3));
        });
    }
}
