<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    public final const LANGUAGES = [
        [
            'locale' => 'EN',
            'prefix' => 'en',
        ],
        [
            'locale' => 'UA',
            'prefix' => 'ua',
        ],
        [
            'locale' => 'RU',
            'prefix' => 'ru',
        ],
    ];


    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach (self::LANGUAGES as $language) {
            Language::query()->firstOrNew($language)->save();
        }
    }
}
