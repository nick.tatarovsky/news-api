<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Tag;
use Illuminate\Support\Arr;

class TagRepository
{
    private const PAGINATE_SIZE = 50;

    public function create(array $data): void
    {
        Tag::create($data);
    }

    public function getList(array $data)
    {
        return Tag::orderBy('created_at')->paginate(
            perPage: Arr::get($data, 'per_page', self::PAGINATE_SIZE),
            page: Arr::get($data, 'page', 1)
        );
    }

    public function update(array $data, Tag $tag): Tag
    {
        $tag->update($data);

        return $tag;
    }

    public function delete(Tag $tag)
    {
        $tag->delete();
    }
}
