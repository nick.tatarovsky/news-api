<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Language;
use App\Models\Post;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;

class PostRepository
{
    private const PAGINATE_SIZE = 10;

    public function firstOrCreate(?int $post_id)
    {
        return Post::firstOrCreate($post_id ? ['id' => $post_id] : []);
    }

    public function getList(array $data, Language $language): LengthAwarePaginator
    {
        return Post::with([
                'translations' => function ($query) use ($language) {
                    $query->where('language_id', $language->id);
                },
                'tags'
            ])
            ->orderBy('created_at')
            ->paginate(
                perPage: Arr::get($data, 'per_page', self::PAGINATE_SIZE),
                page: Arr::get($data, 'page', 1)
            );
    }

    public function get(Language $language, Post $post): Post
    {
        return $post->load([
            'translations' => function ($query) use ($language) {
                $query->where('language_id', $language->id);
            },
            'tags'
        ]);
    }

    public function delete(Post $post): void
    {
        $post->delete();
    }
}
