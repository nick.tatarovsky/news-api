<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Language;
use App\Models\Post;
use App\Models\PostTranslation;

class PostTranslationRepository
{
    public function create(array $data): void
    {
        PostTranslation::create($data);
    }

    public function update(array $data, Language $language, Post $post): Post
    {
        $post->translations()->where('language_id', $language->id)->update($data);

        return $post;
    }

    public function delete(Language $language, Post $post): void
    {
        $post->translations()->where('language_id', $language->id)->delete();
    }
}
