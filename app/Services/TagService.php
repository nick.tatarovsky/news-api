<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Tag;
use App\Repositories\TagRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class TagService
{
    public function __construct(
        private readonly TagRepository $tagRepository,
    ) {}

    public function store(array $data): void
    {
        $this->tagRepository->create($data);
    }

    public function getList(array $data): LengthAwarePaginator
    {
        return $this->tagRepository->getList($data);
    }

    public function update(array $data, Tag $tag): Tag
    {
        return $this->tagRepository->update($data, $tag);
    }

    public function delete(Tag $tag): void
    {
        $this->tagRepository->delete($tag);
    }
}
