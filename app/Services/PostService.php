<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Language;
use App\Models\Post;
use App\Repositories\PostRepository;
use App\Repositories\PostTranslationRepository;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Throwable;

class PostService
{
    public function __construct(
        private readonly PostRepository $postRepository,
        private readonly PostTranslationRepository $postTranslationRepository,
    ) {}

    public function store(array $data, Language $language): void
    {
        DB::beginTransaction();

        try {
            $post = $this->postRepository->firstOrCreate(Arr::get($data, 'post_id'));

            $tags_ids = Arr::pull($data, 'tags_ids');

            $this->postTranslationRepository->create(array_merge($data, [
                'post_id' => $post->id,
                'language_id' => $language->id,
            ]));

            $post->tags()->sync($tags_ids);

            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            abort(500);
        }
    }

    public function getList(array $data, Language $language): LengthAwarePaginator
    {
        return $this->postRepository->getList($data, $language);
    }

    public function show(Language $language, Post $post): Post
    {
        $post = $this->postRepository->get($language, $post);

        if (! $post->translations->count()) {
            throw new HttpResponseException(
                getErrors(['Not found'], 404)
            );
        }

        return $post;
    }

    public function update(array $data, Language $language, Post $post): ?Post
    {
        if ($post->translations()->where('language_id', $language->id)->doesntExist()) {
            throw new HttpResponseException(
                getErrors(['Not found'], 404)
            );
        }

        DB::beginTransaction();

        try {
            $tags_ids = Arr::pull($data, 'tags_ids');

            $this->postTranslationRepository->update($data, $language, $post);

            $post->tags()->sync($tags_ids);

            DB::commit();

            return $post->load([
                'translations' => function ($query) use ($data, $language) {
                    $query->where('language_id', $language->id);
                },
                'tags'
            ]);
        } catch (Throwable $e) {
            DB::rollBack();
            abort(500);
        }
    }

    public function delete(Language $language, Post $post): void
    {
        DB::beginTransaction();

        try {
            $this->postTranslationRepository->delete($language, $post);

            if ($post->translations()->doesntExist()) {
                $post->tags()->detach();
                $this->postRepository->delete($post);
            }

            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            abort(500);
        }
    }
}
