<?php

declare(strict_types=1);

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Response;

function getErrors(array $errors = [], int $code = 400): JsonResponse
{
    return Response::json(['errors' => $errors], $code, [], JSON_UNESCAPED_UNICODE);
}

function getSuccessResponse(Collection|array $data = ['success' => true], int $code = 200): JsonResponse
{
    return Response::json($data, $code, [],
        JSON_UNESCAPED_SLASHES
        | (JSON_UNESCAPED_UNICODE + defined('JSON_INVALID_UTF8_IGNORE') ? 1048576 : 0)
    );
}
