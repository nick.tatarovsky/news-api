<?php

declare(strict_types=1);

namespace App\Http\Resources\Post;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray(Request $request): array
    {
        $translation = $this->translations->first();

        return [
            'id' => $this->id,
            'language_id' => $translation->language_id,
            'title' => $translation->title,
            'description' => $translation->description,
            'content' => $translation->content,
            'created_at' => $translation->created_at,
            'updated_at' => $translation->updated_at,
            'tags_ids' => $this->tags->pluck('id'),
        ];
    }
}
