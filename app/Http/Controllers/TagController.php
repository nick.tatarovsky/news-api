<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\PaginatorRequest;
use App\Http\Requests\Tag\StoreTagRequest;
use App\Http\Requests\Tag\UpdateTagRequest;
use App\Http\Resources\Tag\TagResource;
use App\Models\Tag;
use App\Services\TagService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class TagController extends Controller
{
    public function __construct(
        private readonly TagService $tagService
    ) {}

    public function store(StoreTagRequest $request): JsonResponse
    {
        $this->tagService->store($request->validated());

        return getSuccessResponse(code: 201);
    }

    public function index(PaginatorRequest $request): AnonymousResourceCollection
    {
        $posts = $this->tagService->getList($request->validated());

        return TagResource::collection($posts);
    }

    public function update(UpdateTagRequest $request, Tag $tag): TagResource
    {
        $tag = $this->tagService->update($request->validated(), $tag);

        return TagResource::make($tag);
    }

    public function destroy(Tag $tag): JsonResponse
    {
        $this->tagService->delete($tag);

        return getSuccessResponse();
    }
}
