<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\PaginatorRequest;
use App\Http\Requests\Post\StorePostRequest;
use App\Http\Requests\Post\UpdatePostRequest;
use App\Http\Resources\Post\PostResource;
use App\Models\Language;
use App\Models\Post;
use App\Services\PostService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PostController extends Controller
{
    public function __construct(
        private readonly PostService $postService
    ) {}

    public function store(StorePostRequest $request, Language $language): JsonResponse
    {
        $this->postService->store($request->validated(), $language);

        return getSuccessResponse(code: 201);
    }

    public function index(PaginatorRequest $request, Language $language): AnonymousResourceCollection
    {
        $posts = $this->postService->getList($request->validated(), $language);

        return PostResource::collection($posts);
    }

    public function show(Language $language, Post $post): PostResource
    {
        $post = $this->postService->show($language, $post);

        return PostResource::make($post);
    }

    public function update(UpdatePostRequest $request,  Language $language, Post $post): PostResource
    {
        $post = $this->postService->update($request->validated(), $language, $post);

        return PostResource::make($post);
    }

    public function destroy(Language $language, Post $post): JsonResponse
    {
        $this->postService->delete($language, $post);

        return getSuccessResponse();
    }
}
