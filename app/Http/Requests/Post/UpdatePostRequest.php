<?php

declare(strict_types=1);

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'title' => [
                'nullable',
                'string',
                'max:255',
                Rule::unique('post_translations')
                    ->ignore($this->id)
                    ->where('language_id', $this->input('language'))
            ],
            'description' => 'nullable|string|max:1000',
            'content' => 'nullable|string',
            'tags_ids' => 'nullable|array',
            'tags_ids.*' => 'required|int|exists:tags,id'
        ];
    }
}
