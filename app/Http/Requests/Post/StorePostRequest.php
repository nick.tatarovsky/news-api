<?php

declare(strict_types=1);

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'post_id' => 'nullable|int|exists:posts,id',
            'title' => [
                'required',
                'string',
                'max:255',
                Rule::unique('post_translations')
                    ->where('language_id', $this->input('language'))
            ],
            'description' => 'required|string|max:1000',
            'content' => 'required|string',
            'tags_ids' => 'nullable|array',
            'tags_ids.*' => 'required|int|exists:tags,id'
        ];
    }
}
