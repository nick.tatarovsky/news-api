# News API
## Вимоги
Docker v3

## Встановлення
1. Клонуйте віддалений репізоторій на локальну машину
```bash
git clone https://gitlab.com/nick.tatarovsky/news-api.git
```
2. Перейдіть до кореневої теки проекту
```bash
cd news-api
```
3. Створіть файл .env, скопіювавши файл .env.example
```bash
cp .env.example .env
```
4. Зберіть та запустіть контейнери Docker
```bash
docker-compose up -d
```
5. Встановіть залежності проекту
```bash
docker-compose exec app composer install
```
6. Сгенеруйте ключ застосунку
```bash
docker-compose exec app php artisan key:generate
```

## База даних
За замовченням використовується MySQL в контейнері (Змініть порт MySQL в [docker-compose.yml](docker-compose.yml), якщо потрібно)
```bash
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=main
DB_USERNAME=root
DB_PASSWORD=root
```
Після налаштування підключення до бази даних скиньте налаштування оточення
```bash
docker-compose exec app php artisan config:clear
```
Запустіть міграції з сидером для додавання мов **і з тестовими даними**
```bash
docker-compose exec app php artisan migrate --seed
```
Запустіть міграції і сидер для додавання мов окремо, **якщо тестові дані не потрібні**
```bash
docker-compose exec app php artisan migrate
```
```bash
docker-compose exec app php artisan db:seed --class=LanguageSeeder
```
## Postman колекція
https://drive.google.com/file/d/1YA23H-KdLSs0aRSSSCfeiwygb_e8MEGA/view?usp=sharing
